﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
public class GizmosExt : MonoBehaviour
{

    [MenuItem("my tools/Gizmos switch")]
    // Use this for initialization
    static void GizmosSwitch()
    {
        foreach (StepButtonBase sb in FindObjectsOfType<StepButtonBase>())
        {
            sb.drawGizmo = !sb.drawGizmo;
        }
      
    }

}
[CustomPropertyDrawer(typeof(ReadOnlyAttribute))]
public class ReadOnlyDrawer : PropertyDrawer
{
    public override float GetPropertyHeight(SerializedProperty property,
                                            GUIContent label)
    {
        return EditorGUI.GetPropertyHeight(property, label, true);
    }

    public override void OnGUI(Rect position,
                               SerializedProperty property,
                               GUIContent label)
    {
        GUI.enabled = false;
        EditorGUI.PropertyField(position, property, label, true);
        GUI.enabled = true;
    }
}