﻿// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)
Shader "Custom/Zombie Survival/Select Line Diffuse" {
	Properties{
		_MainTex("Base (RGB) Trans (A)", 2D) = "white" {}
	_LineTex("Line Texture", 2D) = "white" {}
	_Cutoff("Alpha cutoff", Range(0,1)) = 0.5
		//_EffectColor ("Effect Color", Color) = (1.0,1.0,1.0)
		_ScrollSpeed("Scroll Speed", Range(0,2)) = 0.2
		_LineWidth("Line Width", Range(0,1)) = 0.25
		//_EffectAmount ("Effect Amount", Range(0,1)) = 0.5
		_EffectFill("Effect Fill", Range(0,1)) = 0.0
		_IsSelectable("is Selectable", float) = 0
	}
		SubShader{
		Tags{ "Queue" = "AlphaTest" "IgnoreProjector" = "True" "RenderType" = "TransparentCutout" }
		LOD 200

		CGPROGRAM
#pragma surface surf Lambert alphatest:_Cutoff
		sampler2D _MainTex;
	sampler2D _LineTex;
	//fixed3 _EffectColor;
	//fixed _EffectAmount;
	fixed _EffectFill;
	fixed _IsSelectable;
	fixed _ScrollSpeed;
	fixed _LineWidth;
	struct Input {
		float2 uv_MainTex;
		float3 worldPos;
	};
	void surf(Input IN, inout SurfaceOutput o) {
		fixed4 c = tex2D(_MainTex, IN.uv_MainTex);
		o.Albedo = c.rgb;
		o.Alpha = c.a;
		if (_IsSelectable > 0)
		{
			fixed ScrollValue = _ScrollSpeed * 100 * _Time;
			float2 worldUV = float2(IN.worldPos.x, IN.worldPos.y *_LineWidth - ScrollValue);
			fixed4 l = tex2D(_LineTex, worldUV);
			//o.Emission = ((l.rgb * _EffectColor * (1-_EffectFill)) + (_EffectColor * _EffectFill))*_EffectAmount; 
			o.Emission = l.rgb;
		}
		else
		{
			o.Emission = _EffectFill;
		}
	}
	ENDCG
	}
		Fallback "Legacy Shaders/Transparent/Cutout/VertexLit"
}