// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)

Shader "Custom/Labyrinth/Bumped Diffuse Gradient" {
Properties {
	_MainTex ("Base (RGB)", 2D) = "white" {}
	[NoScaleOffset]_BumpMap ("Normalmap", 2D) = "bump" {}
	_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
	[NoScaleOffset]_GradientTex ("Gradient (RGB)", 2D) = "white" {}
	_Scale ("Gradient Scale", Range(0,1)) = 0.1
	_Offset ("Gradient Offset", Range(-1,1)) = 0
}

SubShader {
	Tags {"Queue"="AlphaTest" "IgnoreProjector"="True" "RenderType"="TransparentCutout"}
	LOD 300

CGPROGRAM
#pragma surface surf Lambert alphatest:_Cutoff

sampler2D _MainTex;
sampler2D _GradientTex;
sampler2D _BumpMap;
fixed _Scale;
fixed _Offset;

struct Input {
	float2 uv_MainTex;
	float3 worldPos;
};

void surf (Input IN, inout SurfaceOutput o) {
	fixed4 c = tex2D(_MainTex, IN.uv_MainTex);
	float2 worldUV = float2(IN.worldPos.x, (IN.worldPos.y * _Scale) - (_Offset));
    fixed4 g = tex2D(_GradientTex, worldUV);
	o.Albedo = c.rgb * g.rgb;
	o.Alpha = c.a;
	o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_MainTex));
}
ENDCG  
}

FallBack "Legacy Shaders/Diffuse"
}
