Shader "Custom/Labyrinth/Light Pillar" {
	Properties{
		[NoScaleOffset]_MainTex("Base (RGB) Trans (A)", 2D) = "white" {}
		[NoScaleOffset]_SolutionTex("Soluthion", 2D) = "white" {}
		_Offset("Solution State", Range(0, 1)) = 0

	}

		SubShader{
			Tags {"Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent"}
			LOD 200

		CGPROGRAM
		#pragma surface surf Lambert alpha:fade

		sampler2D _MainTex;
		sampler2D _SolutionTex;
		fixed _Offset;

		struct Input {
			float2 uv_MainTex;
			float2 uv2_SolutionTex;
		};

		void surf(Input IN, inout SurfaceOutput o) {
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex);
			fixed4 s = tex2D(_SolutionTex, IN.uv2_SolutionTex - (0.5 * _Offset));
			o.Emission = c.rgb;
			o.Alpha = c.a * s;
		}
		ENDCG
		}

			Fallback "Legacy Shaders/Transparent/VertexLit"
}