﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldCreator : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }


    public Transform tile;
    [ContextMenu("field")]
    void MakeField()
    {
        for (int i = 0; i < 13; i++)
        {
            for (int j = 0; j < 10; j++)
            {
                Transform tmp = Instantiate(tile, new Vector3(((float)i / 2), 0, ((float)j / 2)), tile.rotation).transform;
                tmp.SetParent(transform);
            }
        }
    }
    public Transform buttonsParent;
    public Transform tilesParent;
    [ContextMenu("Reaname")]
    void Reaname()
    {
        foreach (AxeActivator t in transform.GetComponentsInChildren<AxeActivator>(true))
        {
            if (t == tilesParent || t == buttonsParent) continue;

            t.gameObject.name = "button";
            t.transform.SetParent(buttonsParent);
        }
    }
    public AnimationCurve c;
    [ContextMenu("set settings")]
    void sETsETTINGS()
    {
        foreach (AxeActivator t in transform.GetComponentsInChildren<AxeActivator>(true))
        {

                t.gameObject.GetComponent<AxeActivator>().curve = c;

        }
    }

}
