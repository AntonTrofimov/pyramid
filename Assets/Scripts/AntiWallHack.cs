﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class AntiWallHack : MonoBehaviour
{

    Image myImg;
    public Text myText;
    public float waitTime = 5;
    Coroutine timer;
    Vector3 startpos;
    PlayerPostionTracking player;
    void OnEnable()
    {
        player = FindObjectOfType<PlayerPostionTracking>();
        myImg = GetComponent<Image>();

        if (FindObjectOfType<LiftController>().isActive)
        {
            print("awh lift on");
            myText.text = "PLEASE RETURN TO TRACKED ZONE!";
            myImg.color = new Color(0, 0, 0, 1);
            FindObjectOfType<LiftController>().StopLift();
        }
        else
        {
            myImg.color = new Color(0, 0, 0, 0);
            StartCoroutine(Ext.ImageColor(myImg, new Color(0, 0, 0, 1), 1));
            timer = StartCoroutine(Timer());
            startpos = player.transform.position;
        }


           
    }


    void OnDisable()
    {
        if (FindObjectOfType<LiftController>().isActive)
        {
            print("awh lift off");
            FindObjectOfType<LiftController>().ResumeLift();
        }else
        {
            StopCoroutine(timer);
        }
       // Time.timeScale = 1;
      //  CheckDist();
    }


    void CheckDist()
    {
        if (Vector3.Distance(startpos, player.transform.position) > 0.8f)
        {
            player.KillPlayer();
        }
    }
    


    IEnumerator Timer()
    {
        float t = 0;
        while (t < waitTime)
        {
            myText.text = "PLEASE RETURN TO TRACKED ZONE!\nYOU HAVE " + ((int)(waitTime - t)).ToString() + " SEC";
            t += Time.deltaTime;
            //CheckDist();
            yield return null;
        }
        player.KillPlayer();
    }


}
