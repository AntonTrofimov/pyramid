﻿// "WaveVR SDK 
// © 2017 HTC Corporation. All Rights Reserved.
//
// Unless otherwise required by copyright law and practice,
// upon the execution of HTC SDK license agreement,
// HTC grants you access to and use of the WaveVR SDK(s).
// You shall fully comply with all of HTC’s SDK license agreement terms and
// conditions signed by you and all SDK and API requirements,
// specifications, and documentation provided by HTC to You."

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using wvr;
using WaveVR_Log;

public class ControllerListener : MonoBehaviour
{
    private static string LOG_TAG = "ControllerListenerTest";
    public WVR_DeviceType device = WVR_DeviceType.WVR_DeviceType_HMD;

    public Vector2 touch;
    public int lastbtnindex = 0;
    public bool isControllerConnected = false;

    void Awake()
    {
#if UNITY_EDITOR
        gameObject.SetActive(false);
#endif
    }
    private void HandleConnectionStatus(bool _value)
    {
        isControllerConnected = _value;
    }

    private void HandlePressDownMenu()
    {

    }

    private void HandlePressDownGrip()
    {

    }

    private void HandlePressDownTouchpad()
    {

    }

    private void HandlePressDownTrigger()
    {

    }

    void OnEnable()
    {
        var _ctrllsn = WaveVR_ControllerListener.Instance;

        if (_ctrllsn != null)
        {
            if (device != WVR_DeviceType.WVR_DeviceType_HMD)
            {
                _ctrllsn.Input(device).ConnectionStatusListeners += HandleConnectionStatus;
                _ctrllsn.Input(device).PressDownListenersMenu += HandlePressDownMenu;
                _ctrllsn.Input(device).PressDownListenersGrip += HandlePressDownGrip;
                _ctrllsn.Input(device).PressDownListenersTouchpad += HandlePressDownTouchpad;
                _ctrllsn.Input(device).PressDownListenersTrigger += HandlePressDownTrigger;
            }
        }
    }

    WVR_InputId[] buttonIds = new WVR_InputId[] {
        WVR_InputId.WVR_InputId_Alias1_Menu,
        WVR_InputId.WVR_InputId_Alias1_Grip,
        WVR_InputId.WVR_InputId_Alias1_DPad_Left,
        WVR_InputId.WVR_InputId_Alias1_DPad_Up,
        WVR_InputId.WVR_InputId_Alias1_DPad_Right,
        WVR_InputId.WVR_InputId_Alias1_DPad_Down,
        WVR_InputId.WVR_InputId_Alias1_Volume_Up,
        WVR_InputId.WVR_InputId_Alias1_Volume_Down,
        WVR_InputId.WVR_InputId_Alias1_Touchpad,
        WVR_InputId.WVR_InputId_Alias1_Trigger,
        WVR_InputId.WVR_InputId_Alias1_Digital_Trigger,
        WVR_InputId.WVR_InputId_Alias1_System,
    };

    WVR_InputId[] axisIds = new WVR_InputId[] {
        WVR_InputId.WVR_InputId_Alias1_Touchpad,
        WVR_InputId.WVR_InputId_Alias1_Trigger
    };

    public Vector2 triggerState;
    void Update()
    {
        foreach (WVR_InputId buttonId in buttonIds)
        {
            if (WaveVR_ControllerListener.Instance.Input(device).GetPress(buttonId))
            {
                lastbtnindex = (int)buttonId;
            }
        }


        if (WaveVR_ControllerListener.Instance.Input(device).GetTouch(WVR_InputId.WVR_InputId_Alias1_Touchpad))
        {
            var axis = WaveVR_ControllerListener.Instance.Input(device).GetAxis(WVR_InputId.WVR_InputId_Alias1_Touchpad);
            touch = axis;
        }
        else
        {
            touch = Vector2.zero;
        }


        if (WaveVR_ControllerListener.Instance.Input(device).GetTouch(WVR_InputId.WVR_InputId_Alias1_Trigger))
        {
            var axis = WaveVR_ControllerListener.Instance.Input(device).GetAxis(WVR_InputId.WVR_InputId_Alias1_Trigger);
            triggerState = axis;
        }
        else
        {
            triggerState = Vector2.zero;
        }
    }

}
