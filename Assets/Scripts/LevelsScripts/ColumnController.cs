﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class ColumnController : StepButtonBase
{

    public Transform[] columns;
    List<Transform> tmpColumns = new List<Transform>();
    public float duration=1;
    [ContextMenu("close")]
    public void CloseOtherColumns()
    {
       
        foreach (ColumnController cc in transform.parent.GetComponentsInChildren<ColumnController>())
        {
            if (cc == this) continue;
            if (tmpColumns.Contains(cc.transform)) continue;
            cc.Close();
        }
    }

    public override void InitOnEnable()
    {
        tmpColumns = new List<Transform>();
        tmpColumns.AddRange(columns);
        sfx.clip = AudioHub.instance.column;
    }
    /*
    private void OnCollisionEnter(Collision collision)
    {
        CloseOtherColumns();
        OpenColumns();
    }
    */
    public override void OnTileStep(Transform t)
    {
        if (t == transform)
        {
            CloseOtherColumns();
        OpenColumns();
        }
    }

    public override void OnDrawGizmosOvverride()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireCube(transform.position, new Vector3(0.3f, 0.3f, 0.3f));
    }

    [ContextMenu("open")]
    public void OpenColumns()
    {
        sfx.Play();
        for (int i = 0; i < columns.Length; i++)
        {
            if (columns[i] != null)
            { 

                columns[i].gameObject.SetActive(true);
                StartCoroutine(Ext.MoveObjectLocal(columns[i], new Vector3(columns[i].localPosition.x, 0, columns[i].localPosition.z), duration));
            }
        }
    }

    [ContextMenu("CloseOnlyMe")]
    public void Close()
    {
        StartCoroutine(DelayClose());
    }

    IEnumerator DelayClose()
    {
        yield return new WaitForSeconds(1);
        sfx.Play();
        StartCoroutine(Ext.MoveObjectLocal(transform, new Vector3(transform.localPosition.x, -5, transform.localPosition.z), 1f, () =>
        {
            gameObject.SetActive(false);
        }));
    }

}
