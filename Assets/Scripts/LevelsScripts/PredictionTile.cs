﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PredictionTile : StepButtonBase {

    public float distToPlayer;
    public float range = 6;
    public float koef = 0;
    public AnimationCurve curve;

    public override void OnDrawGizmosOvverride()
    {
        Gizmos.color = Color.white;
        Gizmos.DrawWireSphere(transform.position, 0.15f); 
    }

    /*
    void Update () {


        if (distToPlayer <= range)
        {
            koef = 1 - (distToPlayer / range);

           transform.localPosition = new Vector3(transform.localPosition.x,
             0.1f*(1-koef)
              , transform.localPosition.z);
        }
    
    }
*/
    public override void OnTileStep(Transform tile)
    {
        if (tile == transform)
        {
            transform.GetChild(0).GetComponent<SpikeController>().Acitve();
        }
    }
}
