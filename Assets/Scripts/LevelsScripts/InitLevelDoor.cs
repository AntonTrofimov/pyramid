﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitLevelDoor : StepButtonBase
{

    public Transform door;

    bool isPlay = false;
 
    private void Start()
    {
        sfx.clip = AudioHub.instance.dooropen;
    }

    public override void InitOnEnable()
    {
        door.position = new Vector3(-0.88f, 1.75f, -0.96f);
        isPlay = false;
    }
    public override void OnTileStep(Transform tile)
    {
    
        if (tile == transform)
        {
            OpenDoor();
        }
    }
    
    [ContextMenu("door")]
    public void OpenDoor()
    {
        if (isPlay) return;
        isPlay = true;
        sfx.Play();
        StartCoroutine(Ext.MoveObject(door, new Vector3(door.position.x, 2.1f, door.position.z), 0.5f,()=> {
            vfx.Play();
            StartCoroutine(Ext.MoveObject(door, new Vector3(door.position.x, 4.5f, door.position.z),2f,()=>
            {
             
            }));
        }));
        StartCoroutine(Ext.ShakeObject(LevelsController.instance.initLevel.parent, 3, 0.01f, 0.01f));
            
    }


}
