﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitLevelLight : MonoBehaviour {
    
    Light mylight;
    public float fadeTime = 2;

    private void OnEnable()
    {
        mylight = GetComponent<Light>();
        mylight.intensity = 1;
    }


	IEnumerator FadeLight(bool sign)
    {
        float t = 0;
        while (t < 2/fadeTime)
        {
            t += (Time.deltaTime / fadeTime);

            mylight.intensity =sign? t:1-t;

            yield return null;
        }
    }

    /*Debug*/
    [ContextMenu("disable")]
    public void StartFadeLight()
    {
        StartCoroutine(FadeLight(false));
    }


}
