﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColumnsLevelIniter : MonoBehaviour {

	
	void OnEnable() {
        Invoke("DelayInit",0.1f);
	}
    public int stayActive=4;
    void DelayInit()
    {
        foreach (Transform t in transform)
        {
            t.gameObject.SetActive(false);
            t.localPosition = new Vector3(t.localPosition.x, -5, t.localPosition.z);
        }

        for (int i = 0; i < stayActive; i++)
        {
            Transform t = transform.GetChild(i);
            t.gameObject.SetActive(true);
            t.localPosition = new Vector3(t.localPosition.x, 0, t.localPosition.z);
        }
    }


	
}
