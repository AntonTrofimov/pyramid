﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisibleBridgePoint : MonoBehaviour
{

    PlayerPostionTracking player;
    float distance;
    float prised;
    public float range;
    public float visibleKoef;
    Vector3 playerPos;
    Vector3 myPoint;
    Color tileColor;
    Material tileMat;
    public GameObject Tile;
    GameObject platform;
    float timer;

    Coroutine mvc;
    GameObject myPs;

    bool used = false;
    float playerHeight = 0;
    public AudioSource sfx;
    private void Start()
    {

        sfx = gameObject.AddComponent<AudioSource>();
        sfx.playOnAwake = false;
        sfx.spatialBlend = 1;
        sfx.loop = true;
        sfx.volume = 0.17f;
        
    }

    void OnEnable()
    {
        platform = Tile.transform.GetChild(2).gameObject;
        player = FindObjectOfType<PlayerPostionTracking>();
        tileColor = platform.GetComponent<MeshRenderer>().material.color;
        tileMat = platform.GetComponent<MeshRenderer>().material;
        used = false;
        startTimer = false;
        visibleKoef = 0;
        distance = 0;
        prised = 0;
        myPs = Tile.transform.GetChild(1).gameObject;
        myPs.SetActive(false);
        Tile.transform.rotation = Quaternion.identity;
        playerHeight = FindObjectOfType<PlayerPostionTracking>().transform.localPosition.y;
        // Tile.GetComponent<Collider>().enabled = false;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, range);
    }

    bool startTimer = false;
    public AnimationCurve curve;



    void Update()
    {
        if (used) return;

        playerPos = new Vector3(player.transform.position.x, 0, player.transform.position.z);

        myPoint = new Vector3(transform.position.x, 0, transform.position.z);


        distance = Vector3.Distance(myPoint, playerPos);

        if (transform.position.y < -2) return;

        if (distance < range)
        {

            /*ВАЖНО*/
            if (!sfx.isPlaying)
            {
                sfx.loop = true;
                sfx.clip = AudioHub.instance.platfromSliding;
                sfx.Play();
            }
            visibleKoef = (1 - distance / range) / 2 + ((playerHeight / 2) / player.transform.localPosition.y) / 2;/// 2 + prised / 2;
        }else
        {
            sfx.Stop();
        }

        // tileMat.color = new Color(tileColor.r, tileColor.g, tileColor.b, visibleKoef);

        Tile.transform.localPosition = Vector3.Lerp(Tile.transform.localPosition,
            new Vector3(Tile.transform.localPosition.x, (-10 * (1 - curve.Evaluate(visibleKoef))), Tile.transform.localPosition.z),
            Time.deltaTime * 4);

        if (visibleKoef > 0.85f && !startTimer)
        {
            startTimer = true;

            timer = Time.time;
        }

        if (visibleKoef > 0.85f && startTimer)
        {
            if (Time.time - timer > 1)
            {


                used = true;
                sfx.loop = false;
                sfx.clip = AudioHub.instance.liftArrived;
                sfx.Play();
                StartCoroutine(Ext.ShakeObject(LevelsController.instance.GetCurrentLevelTransform(),0.6f,0.16f,0.16f));
                myPs.SetActive(true);
                tileMat.color = new Color(tileColor.r, tileColor.g, tileColor.b, 1);
                Tile.transform.localPosition = new Vector3(Tile.transform.localPosition.x, 0, Tile.transform.localPosition.z);
                print("|jgfmkldgfmkl");

                // Tile.GetComponent<Collider>().enabled = true;
            }
            else
            {
                
                Tile.transform.rotation = Quaternion.RotateTowards(Tile.transform.rotation, Quaternion.Euler(0, 90, 0), Time.deltaTime * 90);
            }
        }

        if (visibleKoef < 0.85f && startTimer)
        {
            startTimer = false;
            StartCoroutine(RotateTile());
        }


    }

    IEnumerator RotateTile()
    {
        float t = 0;
        while (t < 1.2f)
        {
            t += Time.deltaTime;
            Tile.transform.rotation = Quaternion.RotateTowards(Tile.transform.rotation, Quaternion.Euler(0, 0, 0), Time.deltaTime * 90);
            yield return null;
        }

    }
}
