﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingObj :MonoBehaviour {

    public float speed=360;
    float speedValue = 0;
    public bool isRotating = false;
    private void Start()
    {
        speedValue = speed;
    }
	void Update () {
        if (isRotating)
        {
            transform.Rotate(transform.up, Time.deltaTime * speed);
        }
	}

    [ContextMenu("start")]
    public void StartGears()
    {
        StartCoroutine(FadeSpeed(true));
    }
    [ContextMenu("stop")]
    public void StopGears()
    {
        StartCoroutine(FadeSpeed(false));
    }

    IEnumerator FadeSpeed(bool side)
    {
        isRotating = true;
        float t = 0;
        float timer = 2;
        while (t < timer)
        {
            t += Time.deltaTime;
            speed =  side ? speedValue * (t / timer ): speedValue *( 1 - (t / timer));
            yield return null;
        }
        isRotating = side ? true : false;
    }
}


