﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bridge : StepButtonBase
{
    public PotolokController pc;
    public List<MeshRenderer> mr = new List<MeshRenderer>();

    private void Start()
    {
        mr.AddRange(transform.GetComponentsInChildren<MeshRenderer>(true));
        mr.ForEach((m) => StartCoroutine(Ext.MaterialColor(m.material, new Color(1, 1, 1, 0), 1)));
    }

    public override void OnTileStep(Transform tile)
    {
        if (tile.transform == transform)
        {
            pc.StopTrap();
        }
    }

    private void OnPrised(bool val)
    {
        Vector3 playerlocalPos = transform.InverseTransformPoint(player.transform.position);
        if (playerlocalPos.x < -2)
        {
            mr.ForEach((m) => StartCoroutine(Ext.MaterialColor(m.material, new Color(1, 1, 1, val ? 1 : 0), 1)));
        }
    }

}
