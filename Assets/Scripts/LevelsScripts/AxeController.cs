﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AxeController : MonoBehaviour
{

    float value;
    public float speed = 0;
    public float timeOffset;
    public float amplitude = 90;
    public AnimationCurve curve;
    bool isBzdin = false;
    [ReadOnly]
    public AudioSource sfx;
    private void Start()
    {
        sfx = gameObject.AddComponent<AudioSource>();
        sfx.playOnAwake = false;
        sfx.spatialBlend = 1;
        sfx.clip = AudioHub.instance.axeDraw;
        sfx.minDistance = 0.5f;
        sfx.maxDistance = 1;
      
    }
    void Update()
    {
        value = curve.Evaluate(Mathf.Sin(timeOffset + Time.time * speed));
        value *= amplitude;
        transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y, value);
        if (value>-3 && value<3 && !isBzdin)
        {
            isBzdin = true;
            sfx.Play();
            Invoke("BzdinAllowDelay", 0.2f);
        }
    }

    void BzdinAllowDelay()
    {
        isBzdin = false;
    }
}
