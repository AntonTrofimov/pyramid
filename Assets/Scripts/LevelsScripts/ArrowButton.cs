﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowButton : StepButtonBase
{

    public List<ArrowSpawner> arrows = new List<ArrowSpawner>();
    public bool doNotDropdown = false;

    bool isActivated = false;
    bool pressed = false;
    public Vector3 vfxLocalPosition;

    public override void OnDrawGizmosOvverride()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireCube(transform.position + new Vector3(0.02f, 0, 0.02f), new Vector3(0.3f, 0.2f, 0.3f));
    }
    public override void InitOnEnable()
    {
        isActivated = false;
        gameObject.GetComponent<Collider>().enabled = true;
        transform.localPosition = new Vector3(transform.localPosition.x, 0, transform.localPosition.z);
        sfx.clip = AudioHub.instance.buttonClick;
        transform.GetChild(0).localPosition = Vector3.zero;
    }

    private void Start()
    {
        vfx = VFXHub.instance.buttonClick;
    }
    void ResetButton()
    {
        pressed = false;
        sfx.clip = AudioHub.instance.buttonReset;
        sfx.Play();
        vfx = VFXHub.instance.buttonReset;
        vfx.transform.position = transform.position + vfxLocalPosition;
        vfx.Play();
        StartCoroutine(Ext.MoveObjectLocal(transform.GetChild(0), new Vector3(0, 0, 0), 0.5f));
    }

    public override void OnTileStep(Transform tile)
    {
        if (tile.transform == transform)
        {
            isActivated = true;
            foreach (ArrowSpawner arrow in arrows)
            {
                if (arrow != null)
                {
                    if (!pressed)
                    {

                        pressed = true;
                        sfx.clip = AudioHub.instance.buttonClick;
                        sfx.Play();

                        vfx = VFXHub.instance.buttonClick;
                        vfx.transform.position = transform.position + vfxLocalPosition;
                        vfx.Play();

                        if (transform.childCount > 0)
                        {
                            StartCoroutine(Ext.MoveObjectLocal(transform.GetChild(0), new Vector3(0, -0.1f, 0), 0.5f, () =>
                            {
                                Invoke("ResetButton", 4f);
                            }));
                        }

                        arrow.Activate();
                    }

                }
            }
        }
        else
        {
            if (isActivated && !doNotDropdown)
            {
                gameObject.GetComponent<Collider>().enabled = false;

                StartCoroutine(Ext.MoveObject(transform, new Vector3(transform.position.x, -100, transform.position.z), 10, () =>
                {
                    isActivated = false;
                }));

            }
        }
    }
}