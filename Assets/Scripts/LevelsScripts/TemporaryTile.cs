﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class TemporaryTile : StepButtonBase
{

    public float t = 0;
    public bool stayOnMe = false;
    public float timeToDestroy = 2;
    bool destoyed = false;
    public float destroyValue = 0;
    public int partsCount = 0;
    int currPart = 0;/*
    public override void OnTileStep(Transform tile)
    {
        if (tile == transform)
        {
            stayOnMe = true;

        }
        else
        {
            stayOnMe = false;
        }
    }
    */

    /*ВАЖНО!!!*/
    private void OnCollisionEnter(Collision collision)
    {
        stayOnMe = true;
    }

    private void OnCollisionExit(Collision collision)
    {
        stayOnMe = false;
    }

    public override void OnDrawGizmosOvverride()
    {
        
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(transform.position, new Vector3(0.3f, 0.1f, 0.3f));
    }

    void FixedUpdate()
    {

        if (stayOnMe && !destoyed)
        {
            t += 0.02f;
            t = (float)System.Math.Round((System.Decimal)t, 2);
            if (t % (step) == 0)
            {
                DestroyOnePart();
            }
        }
    }


    private void Update()
    {


        if (t >= timeToDestroy && !destoyed)
        {
            DestroyTile();
        }
        destroyValue = t / timeToDestroy;
    }

    [ContextMenu("Destroy one")]
    void DestroyOnePart()
    {
        sfx.clip = AudioHub.instance.crunch;
        sfx.volume = UnityEngine.Random.Range(0.2f, 0.7f);
        sfx.Play();
        StartCoroutine(Ext.MoveObject(transform.GetChild(currPart), new Vector3(transform.GetChild(currPart).position.x, -20, transform.GetChild(currPart).position.z), 4));
        currPart++;

    }
    public float step = 0;

    [ContextMenu("Destroy")]
    void DestroyTile()
    {
        destoyed = true;
        StartCoroutine(Ext.MoveObject(transform, new Vector3(transform.position.x, -20, transform.position.z), 4));
    }

    public override void InitOnEnable()
    {
        destoyed = false;
        t = 0;
        currPart = 0;
        destroyValue = 0;

        transform.localPosition = new Vector3(transform.localPosition.x, 0, transform.localPosition.z);
        partsCount = transform.childCount;
        if (partsCount == 0) return;

        foreach (Transform t in transform)
        {
            t.localPosition = Vector3.zero;
        }
        step = Ext.NearestRound( timeToDestroy / partsCount,0.02f);
        step = (float)Math.Round((decimal)step, 4);
        sfx.clip = AudioHub.instance.crunch;
    }
}
