﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PredictionController : MonoBehaviour
{

    public List<Transform> objs = new List<Transform>();
    public List<ParticleSystem> spikesPS = new List<ParticleSystem>();
    public Vector3 vfxLocalPos;
    void OnEnable()
    {
        objs.Clear();
        foreach (PredictionTile t in transform.GetComponentsInChildren<PredictionTile>())
        {
            objs.Add(t.transform);
        }
        Invoke("StartPrediction", 1);
    }

    void StartPrediction()
    {
        if (spikesPS.Count == 0)
        {
            foreach (Transform t in objs)
            {
                spikesPS.Add(Instantiate(VFXHub.instance.lastLevelSpikes.gameObject, t.transform.position + vfxLocalPos, VFXHub.instance.lastLevelSpikes.transform.rotation, t).GetComponent<ParticleSystem>());
            }
        }
        StartCoroutine(Prediction());
    }

    IEnumerator Prediction()
    {
        int time = Random.Range(3, 7);
        int count = Random.Range(10, 17);
        int arrayOffset = Random.Range(0, objs.Count - count - 1);
        for (int i = arrayOffset; i < arrayOffset + count; i++)
        {
            StartCoroutine(Ext.ShakeObject(objs[i], time, 0.001f, 0.001f));
            spikesPS[i].Play();
        }
        yield return new WaitForSeconds(2 + time);
        StartCoroutine(Prediction());
        yield break;
    }
}