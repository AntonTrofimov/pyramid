﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GloveAnim : MonoBehaviour
{

    public string state;
    public Transform userContoller;
    public ParticleSystem vfx;
    private void Start()
    {
       userContoller = FindObjectOfType<PlayerPostionTracking>().controller.transform;
    }

    void Flying()
    {
        transform.Rotate(Vector3.up, Time.deltaTime * 40);
        transform.localPosition = new Vector3(0, ((Mathf.Sin(Time.time) + 1) / 2) * 0.135f, 0);
    }

    void FollowHand()
    {
        transform.position = Vector3.Lerp(transform.position, userContoller.position, Time.deltaTime * 15);
        transform.rotation = Quaternion.Lerp(transform.rotation, userContoller.rotation * Quaternion.Euler(0,90,90), Time.deltaTime * 15);
    }

    [ContextMenu("debug GoToHand")]
    public void GoToHand()
    {
        state = null;
        StartCoroutine(Ext.MoveObjectToTransfrom(transform,userContoller,5,()=> { state = "followHand"; }));
    }


    private void Update()
    {
        if (state == "followHand")
        {
            FollowHand();
        }

        if (state == "fly")
        {
            Flying();
        }
    }
}
