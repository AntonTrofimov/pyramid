﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeController : MonoBehaviour {

    public Transform spike;
    public bool isActive = false;
    public AnimationCurve curve;
    public AudioSource sfx;
    public float duration = 2;
    public ParticleSystem vfx;
    public bool showAfterUse = false;
	void OnEnable () {
        spike = transform.GetChild(0);
        spike.localPosition = new Vector3(0, -1.2f, 0);
        spike.gameObject.SetActive(false);
    }

    private void Start()
    {
        sfx = gameObject.AddComponent<AudioSource>();
        sfx.playOnAwake = false;
        sfx.clip = AudioHub.instance.spike;
        vfx = VFXHub.instance.spikeDust;
    }
	
    [ContextMenu("Active")]
    public void Acitve()
    {

        if (isActive) return;

        

        GameObject tmpvfx = Instantiate(vfx.gameObject, transform.position + new Vector3(0, 0.2f, 0), Quaternion.Euler(-90,0,0));
        tmpvfx.GetComponent<ParticleSystem>().Play();
        Destroy(tmpvfx, 1);
        StartCoroutine(SpikeActive());
       
    }

	IEnumerator SpikeActive()
    {
        sfx.Play();
        sfx.volume = 0.5f;
        float t = 0;
        isActive = true;
        spike.gameObject.SetActive(true);
         while (t < duration)
         {
             t+= Time.deltaTime;
             spike.localPosition = new Vector3(0, curve.Evaluate(t/duration), 0);
            yield return null;
         }
      
        isActive = false;
        spike.gameObject.SetActive(false);
        yield break;
    }
}
