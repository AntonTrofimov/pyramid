﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShaketTile : MonoBehaviour {


    private void OnEnable()
    {
        Invoke("StartShake", 1);
    }

    void StartShake()
    {

        StartCoroutine(Ext.ShakeObject(transform, Random.Range(1,4), 0.005f, 0.001f));
        Invoke("StartShake", 4);
    }
}
