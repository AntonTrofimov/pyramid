﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class TilesController : StepButtonBase {

    public override void InitOnEnable()
    {
        foreach (Transform t in transform)
        {
            t.gameObject.SetActive(true);
            if(t.gameObject.GetComponent<Collider>())
            t.GetComponent<Collider>().enabled = true;
            t.localPosition = new Vector3(t.localPosition.x, 0, t.localPosition.z);
        }
    }

    /*public override void OnTileStep(Transform tile)
    { 
        
        if (tile.name == "tile" || tile.name=="hole")
        {

            ClearTilesInRange(tile.position, tile.localScale *0.5f);

        }
    }*/
    public Vector3 overlap;
    private void Update()
    {
        ClearTilesInRange(player.transform.position, overlap);

    }

    public void ClearTilesInRange(Vector3 from,Vector3 range)
    {
        if (!gameObject.activeSelf) return;
        Collider[] hitColliders = Physics.OverlapBox(from, range, Quaternion.identity);
        foreach (Collider col in hitColliders)
        {
            Transform tcol = col.transform;
            if (col.name == "hole")
            {
                tcol.GetComponent<Collider>().enabled = false;
                if(transform.parent.gameObject.activeSelf)
                StartCoroutine(Ext.MoveObject(tcol, new Vector3(tcol.position.x, -10, tcol.position.z), 2, () => { tcol.gameObject.SetActive(false); }));
            }
        }
    }

    [ContextMenu("DestroySecondRow")]
    public void DestroySecondRow()
    {
        ClearTilesInRange(new Vector3(1.773f,0,-.5f), new Vector3(5,2,0.7f));
    }



}
