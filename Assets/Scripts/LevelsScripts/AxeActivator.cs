﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AxeActivator : StepButtonBase
{
    [Header("Axe Button Settings")]
    public HorizontalAxeController axe;
    public float distToPlayer;
    float range = 6;
    float koef = 0;
    public AnimationCurve curve;
    public bool usePrediction = true;
    bool pressed = false;
    public bool moveAxeByZ = true;
    public Vector3 vfxLocalPosition;

    public override void InitOnEnable()
    {
        Vector3 startpos = transform.localPosition;
        sfx.clip = AudioHub.instance.buttonClick;
        vfx = VFXHub.instance.buttonClick;
        transform.GetChild(0).localPosition = Vector3.zero;
        
    }

    void ResetButton()
    {
        sfx.clip = AudioHub.instance.buttonReset;
        sfx.Play();
        vfx = VFXHub.instance.buttonReset;
        vfx.transform.position = transform.position + vfxLocalPosition;
        vfx.Play();
        pressed = false;
        StartCoroutine(Ext.MoveObjectLocal(transform.GetChild(0), new Vector3(0, 0, 0), 0.5f));
    }


    public override void OnDrawGizmosOvverride()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(transform.position+new Vector3(0.02f,0, 0.02f), new Vector3(0.3f, 0.2f, 0.3f));
    }

    public override void OnTileStep(Transform tile)
    {
        if (tile == transform)
        {
            sfx.clip = AudioHub.instance.buttonClick;
            sfx.Play();
            if (axe)
            {
                if (moveAxeByZ)
                {
                    axe.transform.position = new Vector3(axe.transform.position.x, axe.transform.position.y, transform.position.z);
                }
                if (!pressed)
                {
                    axe.Active();
                    vfx = VFXHub.instance.buttonClick;
                    vfx.transform.position = transform.position + vfxLocalPosition;
                    vfx.Play();
                    pressed = true;

                    StartCoroutine(Ext.MoveObjectLocal(transform.GetChild(0), new Vector3(0, -0.1f, 0), 0.5f, () =>
                    {
                        Invoke("ResetButton", 4f);
                    }));
                }
            }
            else
            {
                Debug.LogError("NET TOPORA!");
            }

        }
    }


    private void Update()
    {
        if (usePrediction)
        {

            distToPlayer = Vector3.Distance(new Vector3(player.transform.position.x, 0, player.transform.position.z), new Vector3(transform.position.x, 0, transform.position.z));
            koef = 0;
            if (distToPlayer <= range)
            {
                koef = curve.Evaluate(1 - (distToPlayer / range));

            }

            transform.localPosition = new Vector3(transform.localPosition.x,
                  (1 - koef) / 7f
                   , transform.localPosition.z);
        }

    }
}
