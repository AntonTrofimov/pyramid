﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class ArrowSpawner : MonoBehaviour
{

    LineRenderer lr;
    Transform bullet;
    Vector3 bulletStartLocalPos;
    public bool isActivated = false;
    public float delaySec = 2;
    public AudioSource sfx;
    Material lightPillar;
    GameObject lightPillarObj;
    ParticleSystem vfx;

    public Vector3 vfxLocalPosiotion;
    void OnEnable()
    {
        // Ext.cc.Init();
        //  lr = GetComponent<LineRenderer>();
        //  lr.enabled = false;
        bullet = transform.GetChild(0);

        bullet.gameObject.SetActive(false);
        bullet.localPosition = Vector3.zero;
        isActivated = false;
        lightPillarObj = transform.GetChild(1).GetChild(0).gameObject;
        lightPillar = lightPillarObj.GetComponent<MeshRenderer>().material;
        lightPillarObj.SetActive(false);
        sfx = gameObject.AddComponent<AudioSource>();
        sfx.playOnAwake = false;
        sfx.clip = AudioHub.instance.arrowShoot;
        vfx = VFXHub.instance.shootingRoseLaser;
    }


    IEnumerator LineFlash()
    {
        float t = 0;
        while (t < 1.5f)
        {
            t += Time.deltaTime;
            lightPillar.SetFloat("_Offset", 1 - Mathf.Sin(t * 2));
            yield return null;
        }


    }

    [ContextMenu("Activate")]
    public void Activate()
    {
        if (isActivated) return;
        StartCoroutine(DelayShoot());
    }
    IEnumerator DelayShoot()
    {
        sfx.clip = AudioHub.instance.arrowLaser;
        sfx.volume = 0.4f;
        sfx.Play();


        GameObject tmpvfx = Instantiate(vfx.gameObject, transform.position + vfxLocalPosiotion, Quaternion.Euler(-90, 0, 0));
        tmpvfx.GetComponent<ParticleSystem>().Play();
        Destroy(tmpvfx, 1);

        //lightPillarObj.SetActive(true);

        //StartCoroutine(LineFlash());
        isActivated = true;
        // lr.enabled = true;

        yield return new WaitForSeconds(delaySec);
        //lightPillarObj.SetActive(false);
        // lr.enabled = false;
        Shoot();
    }
    void Shoot()
    {
        sfx.clip = AudioHub.instance.arrowShoot;
        sfx.Play();
        bullet.gameObject.SetActive(true);
        StartCoroutine(Ext.MoveObject(bullet, bullet.TransformPoint(0, 0, 50), 3, () =>
        {
            bullet.gameObject.SetActive(false);
            bullet.localPosition = Vector3.zero;
            isActivated = false;
        }));
    }
}
