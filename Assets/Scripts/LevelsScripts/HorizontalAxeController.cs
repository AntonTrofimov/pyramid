﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum RotateAxis
{
    up,right
}
public class HorizontalAxeController : MonoBehaviour
{

    public float rotSpeed;
    public float duration;
    public int side;
    public bool isActive = false;
    public RotateAxis rotateAxis;
    AudioSource sfx;

    private void Start()
    {
        sfx = GetComponent<AudioSource>();

    }
    // Update is called once per frame
    void Update()
    {
        //transform.RotateAround(Vector3.up, Time.deltaTime * rotSpeed);
    }

    [ContextMenu("Activate")]

    public void Active()
    {
        if (!isActive)
        {
            sfx.Play();
            StartCoroutine(Rotate(duration));
        }
    }

    private void OnEnable()
    {
        transform.rotation = Quaternion.identity;
        isActive = false;
    }

    IEnumerator Rotate(float duration)
    {
        isActive = true;
        Quaternion startRot = transform.rotation;
        float t = 0.0f;
        while (t < duration - 0.1f)
        {
            t += Time.deltaTime;
            if (rotateAxis == RotateAxis.up)
            {
                transform.rotation = startRot * Quaternion.AngleAxis(t / duration * (side * 360f), transform.up); //or transform.right if you want it to be locally based
            }
            if (rotateAxis == RotateAxis.right)
            {
                transform.rotation = startRot * Quaternion.AngleAxis(t / duration * (side * 360f), transform.right); //or transform.right if you want it to be locally based
            }

           
            yield return null;
        }
        isActive = false;
        transform.rotation = Quaternion.identity;
    }


    IEnumerator RotateTile()
    {
        float t = 0;
        while (t < 1f)
        {
            t += Time.deltaTime;
            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(0, side * 179, 0), Time.deltaTime * 179);
            yield return null;
        }

        while (t < 2f)
        {
            t += Time.deltaTime;
            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(0, 2 * -side, 0), Time.deltaTime * 179);
            yield return null;
        }

        transform.eulerAngles = Vector3.zero;


    }

}
