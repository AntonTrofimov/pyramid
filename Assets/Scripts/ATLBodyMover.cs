﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ATLBodyMover : MonoBehaviour {

    Rigidbody rb;
    bool isJump = false;
    public float jumpForce;
	void Start () {
        rb = GetComponent<Rigidbody>();
	}

	void Update () {
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Jump();
        }
#else

#endif


    }
    void jumpswitch() { isJump = false; }

    public void Jump()
    {
        if (isJump) return;
        rb.AddForce(new Vector3(0, 1, 0) * jumpForce, ForceMode.Impulse);
        isJump = true;
        Invoke("jumpswitch", 0.9f);
    }
}
