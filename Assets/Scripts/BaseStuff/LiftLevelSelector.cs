﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LiftLevelSelector : MonoBehaviour
{

    Text myText;
    int selectedLevel = 0;
    void OnEnable()
    {
        myText = transform.GetChild(0).GetComponent<Text>();
        Invoke("UpdateText", 0.1f);
    }

    [ContextMenu("switch lvl")]
    public void SwitchLevel()
    {


        if (LevelsController.instance.currentLevel  < LevelsController.instance.spawnPoints.Count - 2)
        {
            LevelsController.instance.currentLevel++;
        }
        else
        {
            LevelsController.instance.currentLevel = -1;
        }
        UpdateText();
    }

    void UpdateText()
    {

        selectedLevel = LevelsController.instance.currentLevel + 2;
        myText.text = (selectedLevel).ToString();
    }
}
