﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmbientSettings : MonoBehaviour {

    Color oldColor;
	void OnEnable () {
        oldColor = RenderSettings.ambientLight;
        Invoke("SetSettings", 0.2f);
    }

    void SetSettings()
    {
        RenderSettings.ambientLight = Color.black;
    }

    void OnDisable()
    {
        RenderSettings.ambientLight = oldColor;
    }
}
