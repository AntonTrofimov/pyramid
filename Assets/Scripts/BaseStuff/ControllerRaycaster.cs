﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using wvr;

public class ControllerRaycaster : MonoBehaviour
{
    public Image bar;
    Button currBtn;
    void Awake()
    {

#if UNITY_EDITOR
        //  Destroy(this);
#else
      WaveVR_ControllerListener.Instance.Input(wvr.WVR_DeviceType.WVR_DeviceType_Controller_Right).PressDownListenersMenu += HandlePressDownMenu;
                WaveVR_ControllerListener.Instance.Input(wvr.WVR_DeviceType.WVR_DeviceType_Controller_Right).PressUpListenersMenu += HandlePressUpMenu;
#endif



    }

    void Update()
    {
        RaycastHit hit;
        Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 50, Color.red);
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward) * 50, out hit))
        {
            if (hit.transform.GetComponent<Button>())
            {
                currBtn = hit.transform.GetComponent<Button>();
                if (currBtn.transform.childCount > 0)
                {
                    currBtn.transform.GetChild(0).GetComponent<MeshRenderer>().material.SetFloat("_EffectFill", 0.27f);
                }
            }
            else
            {
                currBtn = null;
            }
        }
        else
        {
            currBtn = null;
        }
        if (WaveVR_Controller.Input(WVR_DeviceType.WVR_DeviceType_Controller_Right).GetPressUp(WVR_InputId.WVR_InputId_Alias1_Digital_Trigger))
        {
            if (currBtn != null)
            {
                currBtn.onClick.Invoke();
            }
        }
    }

    Coroutine c;

    void HandlePressDownMenu()
    {
        c = StartCoroutine(WaitGoToInit());
    }

    void HandlePressUpMenu()
    {
        bar.gameObject.SetActive(false);
        StopCoroutine(c);
    }


    IEnumerator WaitGoToInit()
    {
        bar.gameObject.SetActive(true);
        float timer = 3;
        float t = 0;
        bar.fillAmount = 0;
        while (t < timer)
        {
            t += Time.deltaTime;
            bar.fillAmount = t / timer;
            yield return null;
        }
        bar.gameObject.SetActive(false);
        FindObjectOfType<LiftController>().AllowToUseLift();
        LevelsController.instance.GoToInitLevel();
        FindObjectOfType<LiftController>().transform.position = new Vector3(-1.5f, 0, -1.5f);
        yield break;
    }
}