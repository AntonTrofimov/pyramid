﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LevelsController : MonoBehaviour
{
    public static LevelsController instance;
    public Material initLevelSkybox;
    public float shahtaLenght = 50;
    public Transform initLevel;
    public Transform deadLevel;
    public Transform fallZone;
    public List<Transform> spawnPoints = new List<Transform>();
    public Transform player;
    public Transform debugCanvas;
    public int currentLevel = 0;
    public int levelSwitchesCount = 0;
    public AntiWallHack awh;
    public bool startFormInit = false;
    bool canSwitchLevel = true;
    void Awake()
    {
        instance = this;
#if !UNITY_EDITOR
        startFormInit = true;
        currentLevel = -1;
#else
        fallZone.gameObject.SetActive(false);
    
        if (startFormInit)
        {
            GoToInitLevel();
        }
        else
        {
            ReloadLevel();
        }

#endif
    }

    public Transform GetCurrentLevelTransform()
    {
        return spawnPoints[currentLevel].parent;
    }
    IEnumerator Start()
    {
        yield return new WaitForSeconds(0.3f);

    }

    [ContextMenu("next level")]
    void DbgNextLevel()
    {
        NextLevel(true);
    }
    public void NextLevel(bool isSimple = false)
    {

        if (currentLevel < spawnPoints.Count - 1)
        {
            if (isSimple)
            {
                SimpleSelectLevel(++currentLevel);
            }
            else
            {
                SelectLevel(++currentLevel);
            }

        }
        else
        {
            if (isSimple)
            {
                SimpleSelectLevel(0);
            }
            else
            {
                SelectLevel(0);
            }

            print(currentLevel);
        }
    }
    [ContextMenu("dead level")]
    public void GoToDeadLevel()
    {
        SelectLevel(-2);
    }
 
    [ContextMenu("init level")]
    public void GoToInitLevel(bool isSimple = false)
    {
        awh.gameObject.SetActive(false);
        AudioHub.instance.initZone.Transition();
        SelectLevel(-1);
    }


 

    [ContextMenu("prev level")]
    void DbgPrevLevel()
    {
        PrevLevel(true);
    }
    public void PrevLevel(bool isSimple = false)
    {
        if (currentLevel > 0)
        {
            if (isSimple)
            {
                SimpleSelectLevel(--currentLevel);
            }
            else
            {
                SelectLevel(--currentLevel);
            }

        }
        else
        {

            if (isSimple)
            {
                SimpleSelectLevel(spawnPoints.Count - 1);
            }
            else
            {
                SelectLevel(spawnPoints.Count - 1);
            }



        }
    }

    void switchLevelDelay()
    {
        canSwitchLevel = true;
    }

    public void SelectLevel(int level)
    {

        if (!canSwitchLevel) return;
        canSwitchLevel = false;
        Invoke("switchLevelDelay", 0.1f);
        deadLevel.parent.gameObject.SetActive(false);
        initLevel.parent.gameObject.SetActive(false);
        foreach (Transform sp in spawnPoints)
        {
            sp.parent.gameObject.SetActive(false);
        }
        switch (level)
        {
            case -1:
                initLevel.parent.gameObject.SetActive(true);
                initLevel.parent.position = new Vector3(0, 0, 0);
                player.position = initLevel.position;
                levelSwitchesCount = 1;
                RenderSettings.skybox = initLevelSkybox;
                break;

            case -2:
                deadLevel.parent.gameObject.SetActive(true);
                deadLevel.parent.position = new Vector3(0, -200, 0);
                player.position = deadLevel.position;
                RenderSettings.skybox = null;
                break;
            default:
                RenderSettings.skybox = null;
                fallZone.gameObject.SetActive(false);
                spawnPoints[level].parent.gameObject.SetActive(true);
                spawnPoints[level].parent.position = new Vector3(0, 0/*levelSwitchesCount * shahtaLenght*/, 0);
                // player.position = spawnPoints[level].position;
                currentLevel = level;
                //ActiveFallZone();
                levelSwitchesCount++;
                break;
        }
    }


    public void SimpleSelectLevel(int level)
    {
        if (!canSwitchLevel) return;
        canSwitchLevel = false;
        Invoke("switchLevelDelay", 0.1f);
        deadLevel.parent.gameObject.SetActive(false);
        initLevel.parent.gameObject.SetActive(false);
        foreach (Transform sp in spawnPoints)
        {
            sp.parent.gameObject.SetActive(false);
        }
        RenderSettings.skybox = null;
        spawnPoints[level].parent.gameObject.SetActive(true);
        spawnPoints[level].parent.position = new Vector3(0,0 /*levelSwitchesCount * shahtaLenght*/, 0);
        if (currentLevel != 3)
        {
            ActiveFallZone();
        }
        
        player.position = spawnPoints[level].position;
        Transform lift = FindObjectOfType<LiftController>().transform;
        lift.position = new Vector3(lift.position.x, spawnPoints[level].position.y, lift.position.z);
        currentLevel = level;
        levelSwitchesCount++;
    }

    public void ActiveFallZone()
    {
        fallZone.gameObject.SetActive(true);
        fallZone.position = new Vector3(0, 0 /*(levelSwitchesCount - 1) * shahtaLenght*/ - 10.38f, 0);
    }

    void MoveDebugCanvas()
    {
        debugCanvas.SetParent(spawnPoints[currentLevel].parent);
        debugCanvas.localPosition = new Vector3(0, 2f, 5f);
        debugCanvas.localEulerAngles = new Vector3(0, 0, 0);
    }


    [ContextMenu("reload level")]
    public void ReloadLevel()
    {
        SimpleSelectLevel(currentLevel);
    }
}
