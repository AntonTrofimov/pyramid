﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReadOnlyAttribute : PropertyAttribute
{

}



public class StepButtonBase : MonoBehaviour
{
    protected PlayerPostionTracking player;
    [Header("Base settings")]
    public bool drawGizmo;
    [ReadOnly]
    public AudioSource sfx;

    public ParticleSystem vfx;

    void Awake()
    {
        player = FindObjectOfType<PlayerPostionTracking>();
        InitOnStart();

        if (!player)
        {
            return;
        }
        player.onStepOnObj += OnTileStep;

        sfx = gameObject.AddComponent<AudioSource>();
        sfx.playOnAwake = false;
        sfx.spatialBlend = 1;

    }

    void OnEnable()
    {
        if (player)
            player.onStepOnObj += OnTileStep;
       
        InitOnEnable();
    }

    private void OnDrawGizmos()
    {
        if (drawGizmo)
        {
            OnDrawGizmosOvverride();
        }
    }

    public virtual void OnDrawGizmosOvverride() { }

    void OnDisable()
    {
        if (player)
            player.onStepOnObj -= OnTileStep;

        AfterOnDisable();
    }

    public virtual void AfterOnDisable()
    {

    }

    public virtual void OnTileStep(Transform tile) { }

    public virtual void InitOnEnable() { }


    public virtual void InitOnStart() { }

}
