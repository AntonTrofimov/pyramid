﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

[System.Serializable]
public class AudioSnapshotWrapper
{
    public AudioMixerSnapshot snapshot;
    public float Transitiontime = 3;

    public void Transition()
    {
        snapshot.TransitionTo(Transitiontime);
    }

    public void Transition(float time)
    {
        snapshot.TransitionTo(time);
    }
}

public class AudioHub : MonoBehaviour
{
    [SerializeField]
    private AudioClip _buttonClick;

    [SerializeField]
    private AudioClip _buttonReset;

    [SerializeField]
    private AudioClip _spike;
    [SerializeField]
    private AudioClip _arrowShoot;
    [SerializeField]
    private AudioClip _arrowLaser;
    [SerializeField]
    private AudioClip _axeDraw;
    [SerializeField]
    private List<AudioClip> _crunches;
    [SerializeField]
    private AudioClip _column;
    [SerializeField]
    private AudioClip _liftStart;
    [SerializeField]
    private AudioClip _liftArrived;
    [SerializeField]
    private AudioClip _platfromSliding;
    [SerializeField]
    private AudioClip _dooropen;

    public AudioSnapshotWrapper levelZone;
    public AudioSnapshotWrapper liftZone;
    public AudioSnapshotWrapper initZone;


    public static AudioHub instance;


    public AudioClip buttonClick
    {
        get
        {
            return _buttonClick;
        }
    }


    public AudioClip buttonReset
    {
        get
        {
            return _buttonReset;
        }
    }

    public AudioClip spike
    {
        get
        {
            return _spike;
        }
    }
    public AudioClip arrowShoot
    {
        get
        {
            return _arrowShoot;
        }
    }
    public AudioClip arrowLaser
    {
        get
        {
            return _arrowLaser;
        }
    }
    public AudioClip crunch
    {
        get
        {
            return _crunches[Random.Range(0, _crunches.Count - 1)];
        }
    }
    public AudioClip column
    {
        get
        {
            return _column;
        }
    }
    public AudioClip axeDraw

    {
        get
        {
            return _axeDraw;
        }
    }
    public AudioClip liftStart
    {
        get
        {
            return _liftStart;
        }
    }
    public AudioClip liftArrived
    {
        get
        {
            return _liftArrived;
        }
    }
    public AudioClip platfromSliding
    {
        get
        {
            return _platfromSliding;
        }
    }
    public AudioClip dooropen
    {
        get
        {
            return _dooropen;
        }
    }

    void Awake()
    {
        instance = this;
    }
}
