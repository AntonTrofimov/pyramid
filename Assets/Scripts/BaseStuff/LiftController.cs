﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class LiftController : MonoBehaviour
{
    [ReadOnly]
    public bool isActive = false;
    Transform lift;
    public Transform shahta;
    public float distanceToNextLevel = 50;
    public float duration = 10;
    public Transform playerSpace;
    float currposY = 0;
    public bool playerInside = false;
    public ParticleSystem dust;
    public AnimationCurve curve;
    public AnimationCurve curve2;
    public AudioSource sfx;
    public AudioSource mainSfx;
    public float WaitForStartSec = 3;
    public List<RotatingObj> gears = new List<RotatingObj>();
    [ReadOnly]
    public bool canUseLift = true;
    Collider floor;
    bool levelSwitched = false;
    private void Start()
    {
        floor = transform.Find("floor").GetComponent<Collider>();
        lift = transform;
        shahta.gameObject.SetActive(false);
        mainSfx = GetComponent<AudioSource>();
        sfx = gameObject.AddComponent<AudioSource>();
        sfx.playOnAwake = false;
        sfx.spatialBlend = 1;


    }
    Coroutine delayActive;
    Coroutine mainCoroutine;
    [ContextMenu("start lift")]
    public void StartLift()
    {
#if !UNITY_EDITOR
        if (!playerInside) return;
#endif
        if (isActive) return;
        if (FindObjectOfType<InitLevelLight>())
        {
            FindObjectOfType<InitLevelLight>().StartFadeLight();
        }
        AudioHub.instance.liftZone.Transition();
        gears.ForEach((g) => g.StartGears());
        sfx.clip = AudioHub.instance.liftStart;
        sfx.Play();
        mainSfx.Play();
        floor.enabled = true;
        LevelsController.instance.fallZone.gameObject.SetActive(false);
        isActive = true;
        currposY = transform.position.y;
        shahta.gameObject.SetActive(true);
        shahta.position = new Vector3(transform.position.x, currposY - distanceToNextLevel / 2, transform.position.z);
        playerSpace.SetParent(transform);

        mainCoroutine = StartCoroutine(Ext.MoveObjectLocal(lift, new Vector3(transform.position.x, currposY - distanceToNextLevel / 2, transform.position.z), duration / 2, () =>
          {

              SwitchLevel();
              levelSwitched = true;
              shahta.transform.position = new Vector3(transform.position.x, currposY + distanceToNextLevel / 2, transform.position.z);
              transform.position = new Vector3(transform.position.x, currposY + distanceToNextLevel / 2, transform.position.z);
              mainCoroutine = StartCoroutine(Ext.MoveObjectLocal(lift, new Vector3(transform.position.x, currposY, transform.position.z), duration / 2, () => { OnLiftArrived(); }));
          }));

        /*
      StartCoroutine(Ext.MoveObjectByCurve(lift, new Vector3(transform.position.x, currposY - distanceToNextLevel / 2, transform.position.z), duration / 2,curve, () =>
      {
          SwitchLevel();
          shahta.transform.position = new Vector3(transform.position.x, currposY + distanceToNextLevel / 2, transform.position.z);
          transform.position = new Vector3(transform.position.x, currposY + distanceToNextLevel / 2, transform.position.z);
          StartCoroutine(Ext.MoveObjectByCurve(lift, new Vector3(transform.position.x, currposY, transform.position.z), duration / 2, curve2, () => { OnLiftArrived(); }));
      }));*/
    }

    [ContextMenu("pause lift")]
    public void StopLift()
    {
        StopCoroutine(mainCoroutine);
    }


    [ContextMenu("resume lift")]
    public void ResumeLift()
    {
        if (!levelSwitched)
        {
            SwitchLevel();
            levelSwitched = true;
            shahta.transform.position = new Vector3(transform.position.x, currposY + distanceToNextLevel / 2, transform.position.z);
            transform.position = new Vector3(transform.position.x, currposY + distanceToNextLevel / 2, transform.position.z);
            mainCoroutine = StartCoroutine(Ext.MoveObjectLocal(lift, new Vector3(transform.position.x, currposY, transform.position.z), duration / 2, () => { OnLiftArrived(); }));
        }
        else
        {
            mainCoroutine = StartCoroutine(Ext.MoveObjectLocal(lift, new Vector3(transform.position.x, currposY, transform.position.z), duration / 2, () => { OnLiftArrived(); }));
        }
    }



    void OnLiftArrived()
    {
        levelSwitched = false;
        AudioHub.instance.levelZone.Transition();
        gears.ForEach((g) => g.StopGears());
        floor.enabled = false;
        sfx.clip = AudioHub.instance.liftArrived;
        sfx.Play();
        mainSfx.Stop();
        isActive = false;
        shahta.gameObject.SetActive(false);
        print("lift arrived");
        if (LevelsController.instance.currentLevel != 3)
        {
            LevelsController.instance.ActiveFallZone();
        }

        playerSpace.SetParent(null);
        StartCoroutine(Ext.ShakeObject(LevelsController.instance.GetCurrentLevelTransform(), 0.6f, 0.16f, 0.16f));
        dust.Play();
    }

    private void OnCollisionEnter(Collision collision)
    {
        playerInside = true;

        delayActive = StartCoroutine(WaitForStart());
    }
    private void OnCollisionExit(Collision collision)
    {
        if (isActive) return;
        playerInside = false;
        StopCoroutine(delayActive);
    }

    IEnumerator WaitForStart()
    {
        if (!canUseLift) yield break;
        yield return new WaitForSeconds(WaitForStartSec);
        StartLift();

        Debug.Log("ТУТ ВАЖНЫЙ МОМЕНТ!!!!!!!!");
        /***************************/
        canUseLift = false;
        /****************************/
    }

    void SwitchLevel()
    {
        LevelsController.instance.NextLevel();
        print("switch level");
    }


    public void AllowToUseLift()
    {
        canUseLift = true;
    }
}
