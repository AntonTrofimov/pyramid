﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VFXHub : MonoBehaviour {

    public static VFXHub instance;

    [SerializeField]
    private ParticleSystem _buttonClick;

    [SerializeField]
    private ParticleSystem _spikeDust;

    [SerializeField]
    private ParticleSystem _buttonReset;


    [SerializeField]
    private ParticleSystem _shootingRose;



    [SerializeField]
    private ParticleSystem _shootingRoseLaser;




    [SerializeField]
    private ParticleSystem _lastLevelSpikes;

    public ParticleSystem buttonClick
    {
        get
        {
            return _buttonClick;
        }
    }

    public ParticleSystem lastLevelSpikes
    {
        get
        {
            return _lastLevelSpikes;
        }
    }


    public ParticleSystem shootingRose
    {
        get
        {
            return _shootingRose;
        }
    }



    public ParticleSystem buttonReset
    {
        get
        {
            return _buttonReset;
        }
    }

    public ParticleSystem spikeDust
    {
        get
        {
            return _spikeDust;
        }
    }

    public ParticleSystem shootingRoseLaser
    {
        get
        {
            return _shootingRoseLaser;
        }
    }
    private void Awake()
    {
        instance = this;
    }
}
