﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Ext
{
    public static float NearestRound(float x, float delX)
    {
        if (delX < 1)
        {
            float i = (float)Math.Floor(x);
            float x2 = i;
            while ((x2 += delX) < x) ;
            float x1 = x2 - delX;
            return (Math.Abs(x - x1) < Math.Abs(x - x2)) ? x1 : x2;
        }
        else
        {
            return (float)Math.Round(x / delX, MidpointRounding.AwayFromZero) * delX;
        }
    }
    public static IEnumerator MoveObject(Transform trans, Vector3 endPos, float time, Action act = null)
    {
        Vector3 startPos = trans.position;
        var i = 0.0f;
        var rate = 1.0f / time;
        while (i < 1.0f)
        {
            i += Time.deltaTime * rate;
            trans.position = Vector3.Lerp(startPos, endPos, i);
            yield return null;
        }

        trans.position = endPos;
        if (act != null)
        {
            act.Invoke();
        }
    }

    


    public static IEnumerator MoveObjectToTransfrom(Transform trans, Transform endPos, float time, Action act = null)
    {
        Vector3 startPos = trans.position;
        var i = 0.0f;
        var rate = 1.0f / time;
        while (i < 1.0f)
        {
            i += Time.deltaTime * rate;
            trans.position = Vector3.Lerp(startPos, endPos.position, i);
            yield return null;
        }

        trans.position = endPos.position;
        if (act != null)
        {
            act.Invoke();
        }
    }




    public static IEnumerator ShakeObject(Transform t, float dur, float maxX, float maxY)
    {
  
        float counter = 0f;
        Vector3 startpos = t.position;
        while (counter < dur)
        {
            counter += Time.deltaTime;
    
            t.position = startpos + new Vector3((dur - counter) * UnityEngine.Random.Range(-maxX, maxX), (dur - counter) * 
                UnityEngine.Random.Range(-maxY, maxY));
            yield return null;
        }
        t.position = startpos;
    }


    public static IEnumerator MoveObjectByCurve(Transform trans, Vector3 endPos, float time, AnimationCurve curve, Action act = null)
    {
        Vector3 startPos = trans.position;
        var i = 0.0f;
        var rate = 1.0f / time;
        while (i < 1.0f)
        {
            i += Time.deltaTime * rate;
            trans.position = Vector3.Lerp(startPos, endPos, curve.Evaluate(i));
            yield return null;
        }

        trans.position = endPos;
        if (act != null)
        {
            act.Invoke();
        }
    }


    public static IEnumerator MaterialColor(Material mat, Color endCol, float time, Action act = null)
    {
        Color startPos = mat.color;
        var i = 0.0f;
        var rate = 1.0f / time;
        while (i < 1.0f)
        {
            i += Time.deltaTime * rate;
            mat.color = Color.Lerp(startPos, endCol, i);
            yield return null;
        }

        mat.color = endCol;
        if (act != null)
        {
            act.Invoke();
        }
    }

    public static IEnumerator ImageColor(Image img, Color endCol, float time, Action act = null)
    {
        Color startPos = img.color;
        var i = 0.0f;
        var rate = 1.0f / time;
        while (i < 1.0f)
        {
            i += Time.deltaTime * rate;
            img.color = Color.Lerp(startPos, endCol, i);
            yield return null;
        }

        img.color = endCol;
        if (act != null)
        {
            act.Invoke();
        }
    }



    public static IEnumerator MoveObjectLocal(Transform trans, Vector3 endPos, float time, Action act = null)
    {
        Vector3 startPos = trans.localPosition;
        var i = 0.0f;
        var rate = 1.0f / time;
        while (i < 1.0f)
        {
            i += Time.deltaTime * rate;
            trans.localPosition = Vector3.Lerp(startPos, endPos, i);
            yield return null;
        }

        trans.localPosition = endPos;
        if (act != null)
        {
            act.Invoke();
        }
    }

    public static Vector3 SignedEuler(Vector3 rot)
    {
        float angle = rot.x;
        angle = (angle > 180) ? angle - 360 : angle;
        float angle1 = rot.y;
        angle1 = (angle1 > 180) ? angle1 - 360 : angle1;
        float angle2 = rot.z;
        angle2 = (angle2 > 180) ? angle2 - 360 : angle2;
        Vector3 newRot = new Vector3(angle, angle1, angle2);
        return newRot;
    }



}
