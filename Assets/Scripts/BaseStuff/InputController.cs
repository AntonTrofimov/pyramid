﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using wvr;

public class InputController : MonoBehaviour
{

    public static Quaternion controllerRotation;
    public static Vector3 playerPosition;
    public static Vector3 playerHeadRotation;
    public static Vector3 touchPostion;
    public static bool triggerPressed = false;
    public static bool triggerRealese = false;
    public static bool backBtnPressed = false;
    public static bool backBtnRealese = false;
    public static bool touchpadPressed = false;

    private void Start()
    {
#if !UNITY_EDITOR
        var _ctrllsn = WaveVR_ControllerListener.Instance;

        if (_ctrllsn != null)
        {
            // _ctrllsn.Input(WVR_DeviceType.WVR_DeviceType_Controller_Right).PressDownListenersMenu += HandlePressDownMenu;
            // _ctrllsn.Input(WVR_DeviceType.WVR_DeviceType_Controller_Right).PressDownListenersGrip += HandlePressDownGrip;
            _ctrllsn.Input(WVR_DeviceType.WVR_DeviceType_Controller_Right).PressDownListenersTouchpad += HandlePressDownTouchpad;
            _ctrllsn.Input(WVR_DeviceType.WVR_DeviceType_Controller_Right).TouchUpListenersTouchpad += HandlePressUpTouchpad;
            // _ctrllsn.Input(WVR_DeviceType.WVR_DeviceType_Controller_Right).PressDownListenersTrigger += HandlePressDownTrigger;
        }
#endif
    }

    private void Update()
    {
#if UNITY_EDITOR
        DebugMoveType();
#else
     WaveType();
#endif

    }

    void OVRMoveType()
    {
        /*
        controllerRotation = OVRInput.GetLocalControllerRotation(OVRInput.GetActiveController());
        Vector2 tmptTouchPostion = OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad);
        touchPostion = new Vector3(tmptTouchPostion.x, 0, tmptTouchPostion.y);
        touchpadPressed = OVRInput.Get(OVRInput.Button.PrimaryTouchpad);
        triggerPressed = OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger);
        backBtnPressed = OVRInput.GetDown(OVRInput.Button.Back);
        backBtnRealese = OVRInput.GetUp(OVRInput.Button.Back);
        triggerRealese = OVRInput.GetUp(OVRInput.Button.PrimaryIndexTrigger);
        */
    }


    void WaveType()
    {
        Vector2 tmptTouchPostion;
        if (WaveVR_ControllerListener.Instance.Input(WVR_DeviceType.WVR_DeviceType_Controller_Right).GetTouch(WVR_InputId.WVR_InputId_Alias1_Touchpad))
        {
            var axis = WaveVR_ControllerListener.Instance.Input(WVR_DeviceType.WVR_DeviceType_Controller_Right).GetAxis(WVR_InputId.WVR_InputId_Alias1_Touchpad);
            tmptTouchPostion = axis;
            touchPostion = new Vector3(tmptTouchPostion.x, 0, tmptTouchPostion.y);

        }
        else
        {
        
            touchPostion = Vector3.zero;
        }

        triggerRealese = WaveVR_ControllerListener.Instance.Input(WVR_DeviceType.WVR_DeviceType_Controller_Right).GetPress(WVR_InputId.WVR_InputId_Alias1_Touchpad);
        //backBtnRealese = WaveVR_Controller.Input(WVR_DeviceType.WVR_DeviceType_Controller_Right).GetPressUp(WVR_InputId.WVR_InputId_Alias1_Menu);

        /*
        controllerRotation = WaveVR_ControllerListener.Instance.Input(wvr.WVR_DeviceType.WVR_DeviceType_Controller_Right).;
        touchpadPressed = OVRInput.Get(OVRInput.Button.PrimaryTouchpad);
        triggerPressed = OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger);
        backBtnPressed = OVRInput.GetDown(OVRInput.Button.Back);
        backBtnRealese = OVRInput.GetUp(OVRInput.Button.Back);
        triggerRealese = OVRInput.GetUp(OVRInput.Button.PrimaryIndexTrigger);
        */
    }


    void DebugMoveType()
    {
        controllerRotation = Quaternion.identity;
        touchPostion = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        triggerPressed = Input.GetMouseButtonUp(1);
        backBtnPressed = Input.GetKeyDown(KeyCode.Backspace);
        backBtnRealese = Input.GetKeyUp(KeyCode.Backspace);

        triggerRealese = Input.GetMouseButtonUp(1);
    }

    private void HandlePressDownTouchpad()
    {

    }

    private void HandlePressUpTouchpad()
    {

    }

}
