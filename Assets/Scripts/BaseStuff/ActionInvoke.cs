﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ActionInvoke : StepButtonBase
{
    public AudioClip actSfx;
    public UnityEvent CollisonEvent;
    public UnityEvent OnEnableEvent;
    public UnityEvent OnDisableEvent;

    [ContextMenu("invoke enable")]
    public override void InitOnEnable()
    {
        OnEnableEvent.Invoke();
        sfx.clip = actSfx;
    }

    [ContextMenu("invoke disable")]
    public override void AfterOnDisable()
    {
        OnDisableEvent.Invoke();
    }

    [ContextMenu("player step")]
    void DbgInvoke()
    {
        CollisonEvent.Invoke();
    }
    /*
    private void OnCollisionEnter(Collision collision)
    {

            if (sfx.clip != null)
            {
                sfx.Play();
            }

            CollisonEvent.Invoke();
        
    }
    */
    public override void OnTileStep(Transform tile)
    {
        
        if (tile == transform)
        {
            if (sfx.clip != null)
            {
                sfx.Play();
            }
            
            CollisonEvent.Invoke();
        }
    }

}
