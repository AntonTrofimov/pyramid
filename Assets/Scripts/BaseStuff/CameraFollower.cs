﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollower : MonoBehaviour {

    public Vector3 offset;
    public Transform head;
	// Update is called once per frame
	void Update () {
        transform.position = Vector3.Lerp(transform.position, head.position + offset, Time.deltaTime * 10);
	}

    public void ChangeX(float val)
    {
        offset.x += val;
    }

    public void ChangeY(float val)
    {
        offset.y += val;
    }
    public void ChangeZ(float val)
    {
        offset.z += val;
    }
}
