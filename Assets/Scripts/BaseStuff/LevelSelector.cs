﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSelector : MonoBehaviour
{

    public GameObject ray;
    public GameObject point;
    Transform player;

    private void OnEnable()
    {
        player = FindObjectOfType<PlayerPostionTracking>().transform;
        foreach (Transform t in transform)
        {
            StartCoroutine(Ext.MoveObjectLocal(t, new Vector3(0, t.localPosition.y, 0), 0.3f));
        }
        StartCoroutine(Ext.MoveObjectLocal(transform.GetChild(LevelsController.instance.currentLevel + 1), transform.GetChild(LevelsController.instance.currentLevel + 1).localPosition + new Vector3(0, 0, 0.15f), 0.3f));
    }

    [ContextMenu("2")]
    void Next()
    {
        SelectLevel(2);
    }
    private void Update()
    {
        if (Vector3.Distance(player.position, transform.position) < 2f)
        {
            ray.SetActive(true);
            point.SetActive(true);
        }
        else
        {
            ray.SetActive(false);
            point.SetActive(false);
        }
        foreach (Transform t in transform)
        {
            float val = t.GetChild(0).GetComponent<MeshRenderer>().material.GetFloat("_EffectFill") - Time.deltaTime;
            if (val < 0)
            {
                val = 0;
            }
            t.GetChild(0).GetComponent<MeshRenderer>().material.SetFloat("_EffectFill", val);
        }

    }



    public void SelectLevel(int level)
    {
        foreach (Transform t in transform)
        {
            if (t.GetSiblingIndex() != level)
            {
                StartCoroutine(Ext.MoveObjectLocal(t, new Vector3(0, t.localPosition.y, 0), 0.3f));
            }
          
        }
        StartCoroutine(Ext.MoveObjectLocal(transform.GetChild(level),new Vector3(0, transform.GetChild(level).localPosition.y, 0.15f), 0.3f));

        LevelsController.instance.currentLevel = level - 1;
    }
}
