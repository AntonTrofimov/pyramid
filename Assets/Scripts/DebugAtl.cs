﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugAtl : MonoBehaviour {



    Text myText;
    PlayerPostionTracking cr;
    float deltaTime = 0.0f;
    ControllerListener cl;
    private void Start()
    {
        myText = GetComponent<Text>();
        cr = FindObjectOfType<PlayerPostionTracking>();
        cl = FindObjectOfType<ControllerListener>();
    }

    void Update()
    {
        deltaTime += (Time.unscaledDeltaTime - deltaTime) * 0.1f;
    }

    void OnGUI()
    {
        float fps = 1.0f / deltaTime;
        myText.text = "fps: " + fps.ToString("##.##");
        myText.text += "\ndie:  " + cr.canDie.ToString();


        //myText.text += "\ntouch:  " + cl.touch.ToString();
        //myText.text += "\nlastBtn:  " + cl.lastbtnindex.ToString();
        //myText.text += "\ntrigger:  " + cl.triggerState.ToString();
    }
}
