﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FinalSceneController : MonoBehaviour {
    public Image whiteness;
    public Transform userContoller;
    public GloveAnim glove;
    public Transform artifactSpace;
    public AudioSource sfx;
    bool isPlay = false;
    void Start () {
        userContoller = FindObjectOfType<PlayerPostionTracking>().controller.transform;
      //  StartEndScene();
    }

    [ContextMenu("Start final Scene")]
    public void StartEndScene()
    {
        if (isPlay) return;
        StartCoroutine(EndScene());
    }


    IEnumerator EndScene()
    {

        isPlay = true;
        whiteness.gameObject.SetActive(true);
        glove.state = null;
        StartCoroutine(MoveObjectToTransfrom(glove.transform, userContoller, 5));
        yield return StartCoroutine(Ext.MoveObjectToTransfrom(glove.transform, userContoller, 5, () => { glove.state = "followHand"; }));
        glove.transform.SetParent(null);

        yield return new WaitForSeconds(3);
        glove.vfx.Play();
        sfx.Play();
        yield return new WaitForSeconds(3);
        whiteness.color = new Color(1, 1, 1, 0);
        StartCoroutine(Ext.ImageColor(whiteness, new Color(1, 1, 1, 1), 3));
        yield return StartCoroutine(Ext.ShakeObject(LevelsController.instance.GetCurrentLevelTransform(),10, 0.01f,0.01f));
        glove.transform.SetParent(artifactSpace);
        glove.transform.localPosition = Vector3.zero;
        LevelsController.instance.GoToInitLevel();

        FindObjectOfType<LiftController>().AllowToUseLift();
        StartCoroutine(Ext.ImageColor(whiteness, new Color(0,0, 0, 1f), 1,()=> {
            StartCoroutine(Ext.ImageColor(whiteness, new Color(0, 0, 0, 0f), 2, () => {
               
                glove.state = "fly";
                glove.transform.rotation = Quaternion.identity;
                whiteness.gameObject.SetActive(false);
                glove.vfx.Stop();
                isPlay = false;
            }));
        }));
    }


    public IEnumerator MoveObjectToTransfrom(Transform trans, Transform endPos, float time)
    {
        Quaternion startPos = trans.rotation;
        var i = 0.0f;
        var rate = 1.0f / time;
        while (i < 1.0f)
        {
            i += Time.deltaTime * rate;
            trans.rotation = Quaternion.Lerp(startPos, endPos.rotation * Quaternion.Euler(90, 0, 0), i);
            yield return null;
        }
    }



}
