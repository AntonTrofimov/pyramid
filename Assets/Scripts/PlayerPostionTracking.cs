﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPostionTracking : MonoBehaviour
{

    CapsuleCollider myCollider;
    public Transform steps;
    Transform tracker;
    public bool isGrounded = true;
    public bool isDead = false;
    public bool canDie = false;
    public Vector3 lowerPoint;
    public GameObject antiWallHackCanvas;
    public GameObject controller;
    public AudioSource sfx;
    [HideInInspector]
    public bool canMove = true;

    float currYpos = 0;
    public float timeInFlight = 3;
    public float distanceOfFlight = 10;
    public GameObject pointLight;
    public float pointLightMaxRange = 15;
    private void Awake()
    {
#if UNITY_EDITOR

        WaveVRHead.gameObject.SetActive(false);
        WaveVRHead = debugHead;

#else
        controller.SetActive(true);
          debugHead.gameObject.SetActive(false);
#endif

    }

    void Start()
    {
        myCollider = GetComponent<CapsuleCollider>();
        tracker = transform.parent;
        pointLight.GetComponent<Light>().range = 0;
        pointLight.SetActive(false);

    }


    void JumpDisable()
    {
        //tracker.isJump = false;
    }


    void OnCollisionStay(Collision collision)
    {

        if (collision.gameObject.tag == "wall")
        {
            if (!antiWallHackCanvas.activeSelf)
            {
                antiWallHackCanvas.SetActive(true);
            }

        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "wall")
        {
            antiWallHackCanvas.SetActive(false);
        }
    }

    Quaternion rotation;
    public Transform WaveVRHead;
    public Transform debugHead;
    public void DebugStuff()
    {
        if (Input.GetMouseButton(0))
        {
            Vector3 euler = rotation.eulerAngles;
            rotation = Quaternion.Euler(Mathf.Max(-89f, Mathf.Min(89f, Mathf.DeltaAngle(0, euler.x))), euler.y + 2 * Input.GetAxis("Mouse X"), 0f);
            transform.localRotation = rotation;
        }

        transform.localPosition = new Vector3(transform.localPosition.x, 1.6f, transform.localPosition.z);
        transform.localPosition += new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical")) * 0.03f;
    }





    void Update()
    {
#if UNITY_EDITOR
        //   DebugStuff();
#endif

        if (!canMove) return;
        transform.localPosition = WaveVRHead.localPosition;
        //if (InputController.triggerRealese)
        //{
        //    canDie = !canDie;
        //}
        /*
        if (InputController.backBtnPressed)
        {
            LevelsController.instance.GoToInitLevel();
            FindObjectOfType<LiftController>().transform.position = new Vector3(-1.5f, 0, -1.5f);
        }
        */
        lowerPoint = new Vector3(transform.position.x, transform.position.y - transform.localPosition.y, transform.position.z);
        steps.eulerAngles = new Vector3(steps.eulerAngles.x, WaveVRHead.eulerAngles.y, steps.eulerAngles.z);

        RaycastHit hit;
        Vector3 stepPos = new Vector3(transform.position.x, steps.position.y, transform.position.z);

        if (Physics.Raycast(transform.position, transform.TransformDirection(-Vector3.up), out hit, 3))
        {
            stepPos.y = hit.point.y + 0.05f;
        }

        steps.position = stepPos;



        //  steps.transform.position = lowerPoint;
        if (!isDead)
        {
            myCollider.height = transform.localPosition.y;
            myCollider.center = new Vector3(0, -myCollider.height / 2 - 0.2f, 0);

            if (!canDie) return;

            if (CheckGround(lowerPoint, 0.15f))
            {
                KillPlayer();
            }
        }
    }
    Transform lastHitTransform = null;
    public delegate void OnStepOnObj(Transform tile);
    public OnStepOnObj onStepOnObj;
    void OnCollisionEnter(Collision col)
    {

        if (lastHitTransform == col.transform)
        {
            return;
        }
        else
        {
            lastHitTransform = col.transform;
        }
        if (onStepOnObj != null)
        {
            onStepOnObj(col.transform);
        }

        if (col.gameObject.name == "trap")
        {
            FindObjectOfType<PlayerPostionTracking>().KillPlayer(false);
            return;
        }
    }

    public bool drawOverlapZone = true;
    private void OnDrawGizmos()
    {
        if (drawOverlapZone)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawCube(lowerPoint, new Vector3(0.3f, 1f, 0.3f));
        }
    }
    bool CheckGround(Vector3 center, float radius)
    {
        Collider[] hitColliders = Physics.OverlapBox(center, new Vector3(0.15f, 0.5f, 0.15f));
        /*
        foreach (Collider c in hitColliders)
        {
            print(c.transform.name);
        }
        */
        if (hitColliders.Length == 0)
        {
            return true;
        }
        if (hitColliders.Length == 1 && hitColliders[0].transform == transform)
        {
            return true;

        }
        return false;
    }


    [ContextMenu("kill")]
    void dbgKill()
    {
        KillPlayer();
    }


    IEnumerator PointLightScale()
    {
        pointLight.SetActive(true);
        float t = 0;
        while (t < 1)
        {
            t += Time.deltaTime;
            pointLight.GetComponent<Light>().range = pointLightMaxRange * t;
            yield return null;
        }
    }

    public void KillPlayer(bool fall = true)
    {
        if (canDie)
        {
            if (fall)
            {
                tracker.transform.parent.SetParent(null);
                //sfx.Play();

                currYpos = tracker.transform.position.y;
                isDead = true;
                StartCoroutine(PointLightScale());
                StartCoroutine(Ext.MoveObject(tracker.transform.parent,
                    new Vector3(tracker.transform.parent.position.x, currYpos - distanceOfFlight, tracker.transform.parent.position.z), timeInFlight, () =>
                 {

                     sfx.Play();
                     pointLight.GetComponent<Light>().range = 0;
                     pointLight.SetActive(false);

                     LevelsController.instance.GoToDeadLevel();
                 }));
            }
            else
            {
                tracker.transform.parent.SetParent(null);
                sfx.Play();
                LevelsController.instance.GoToDeadLevel();
            }
        }
    }



    [ContextMenu("reset")]
    public void ResetPlayer()
    {
        //tracker.transform.parent.position = new Vector3(tracker.transform.parent.position.x, 0, tracker.transform.parent.position.z);
        isDead = false;
        LevelsController.instance.ReloadLevel();
    }
}
