﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingCamera : MonoBehaviour {
#if UNITY_EDITOR
    Quaternion rotation;
    public float mouseSensitivity = 2;
    public bool moveInCamDirection = true;
    public float speed = 0.05f;
	
	void Update () {

        if (Input.GetMouseButton(0))
        {
            Vector3 euler = rotation.eulerAngles;
            rotation = Quaternion.Euler(Mathf.Max(-89f, Mathf.Min(89f, Mathf.DeltaAngle(0, euler.x) - mouseSensitivity * Input.GetAxis("Mouse Y"))), euler.y + mouseSensitivity * Input.GetAxis("Mouse X"), 0f);
            transform.localRotation = rotation;
        }

        Vector3 moveVector;
        moveVector = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        if (moveInCamDirection)
        {
            moveVector = Quaternion.Euler(0, transform.eulerAngles.y, 0) * moveVector;
        }
        transform.position += (moveVector * speed);
    }
#endif
}
