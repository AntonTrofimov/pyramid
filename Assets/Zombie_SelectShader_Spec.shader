// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)

Shader "Custom/Zombie Survival/Select Line BumpSpec" {
Properties {
	_Shininess ("Shininess", Range (0.03, 1)) = 0.078125
	_MainTex ("Base (RGB) Gloss (A)", 2D) = "white" {}
	[NoScaleOffset] _BumpMap ("Normalmap", 2D) = "bump" {}
	
	_LineTex ("Line Texture", 2D) = "white" {}
	_ScrollSpeed ("Scroll Speed", Range(0,2)) = 0.2
	_LineWidth ("Line Width", Range(0,1)) = 0.25
	_EffectFill ("Effect Fill", Range(0,1)) = 0.0
	_IsSelectable("is Selectable", Float) = 0
}

SubShader {
	Tags {"RenderType"="Opaque" }
	LOD 250
	
CGPROGRAM
#pragma surface surf MobileBlinnPhong exclude_path:prepass nolightmap noforwardadd halfasview interpolateview

inline fixed4 LightingMobileBlinnPhong (SurfaceOutput s, fixed3 lightDir, fixed3 halfDir, fixed atten)
{
	fixed diff = max (0, dot (s.Normal, lightDir));
	fixed nh = max (0, dot (s.Normal, halfDir));
	fixed spec = pow (nh, s.Specular*128) * s.Gloss;
	
	fixed4 c;
	c.rgb = (s.Albedo * _LightColor0.rgb * diff + _LightColor0.rgb * spec) * atten;
	UNITY_OPAQUE_ALPHA(c.a);
	return c;
}

sampler2D _MainTex;
sampler2D _BumpMap;
half _Shininess;
sampler2D _LineTex;
fixed _EffectFill;
fixed _ScrollSpeed;
fixed _LineWidth;
fixed _IsSelectable;

struct Input {
	float2 uv_MainTex;
	float3 worldPos;
};

void surf (Input IN, inout SurfaceOutput o) {
	fixed4 c = tex2D(_MainTex, IN.uv_MainTex);
	o.Albedo = c.rgb;
	o.Alpha = c.a;	
	o.Gloss = c.a;
	o.Specular = _Shininess;
	o.Normal = UnpackNormal (tex2D(_BumpMap, IN.uv_MainTex));

	if(_IsSelectable > 0)
	{
		fixed ScrollValue = _ScrollSpeed*100 * _Time;
		float2 worldUV = float2(IN.worldPos.x, IN.worldPos.y *_LineWidth - ScrollValue);
		fixed4 l = tex2D(_LineTex, worldUV);
		o.Emission = l.rgb;
	}
	else
	{
		o.Emission = _EffectFill;
	}

}
ENDCG
}

Fallback "Mobile/VertexLit"
}
